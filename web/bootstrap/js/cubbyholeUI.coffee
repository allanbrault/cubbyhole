$ ->
	/// BLOCK HELPERS ///
	

	if Function.prototype.name == undefined and Object.defineProperty != undefined
		Object.defineProperty Function.prototype, 'name', {
			get: () ->
				funcNameRegex = /function\s([^(]{1,})\(/
				results = (funcNameRegex).exec (this).toString()
				if results and results.length > 1 then return results[1].trim() else return "" 
			set: (value)->
		}

	if Math.trunc == undefined
		Math.trunc = (n) ->
			n - n % 1
		

	Array::last ?= (n) ->
  		if n? then @[(Math.max @length - n, 0)...] else @[@length - 1]

	formatDate = (date) ->
		minutes = date.getMinutes()
		hours = date.getHours()
		day = date.getDate()
		month = date.getMonth() + 1
		year = date.getFullYear()
		minutes = '0' + minutes if minutes < 10
		hours = '0' + hours if hours < 10
		day = '0' + day if day < 10
		month = '0' + month if month < 10
		return day + '/' + month + '/' + year + ' ' + hours + ':' + minutes


	readableBytes = (bytes) ->
		end = false
		buffer = bytes
		count = 0
		while not end
			buffer = buffer / 1024
			if buffer < 1
				end = true
			else
				++count
		unitsTab = ['octets', 'Kio', 'Mio', 'Gio', 'Tio', 'Pio', 'Eio', 'Zio', 'Yio']
		unit = unitsTab[count]
		bytes = (Math.trunc (bytes / Math.pow 1024, count) * 100) / 100
		return "#{bytes} #{unit}"

	/// END BLOCK ///

	class Entity
		constructor: (entity, parent) ->
			if not parent and entity.id > 0
				throw "Entity : parent is not defined"
			@id = entity.id
			@name = ko.observable entity.name
			@previousName = ko.observable null
			@link = ko.observable 0
			if typeof entity.updated_at == 'string'
				date = entity.updated_at.split('T')[0].split('-')
				time = entity.updated_at.split('T')[1].split(':')
				gmt = time[2].split('+')[1]
				time[2] = time[2].split('+')[0]
				@updated_at = ko.observable new Date(date[0], date[1], date[2], time[0], time[1], time[2])
			else
				@updated_at = ko.observable new Date entity.updated_at
			@parent = parent
			
			@path = () ->
				pathArr = []
				entity = @.parent
				if entity?
					while entity
						if entity.id != 0
							pathArr.unshift entity.name()
						entity = entity.parent
					pathArr.push @.name()
					path = '#/files/' + pathArr.join '/' 
					if pathArr.length > 0
						path = path + '/'
					return path
				else
					return path = '#/files/'


			@formatted =
				updated_at : ko.computed(
					() -> formatDate @updated_at()
					this
				)

			@isSelected = ko.observable false


	class File extends Entity
		constructor: (entity, parent) ->
			super entity, parent
			@size = entity.file_size
			@uniq_id = entity.uniq_id
			@formatted['size'] = readableBytes @size
			@mime = entity.mime
			@type = 'inconnu'
			if @mime?
				@type = if entity.mime.split('/')[0] == 'image' then entity.mime.split('/')[0] else @name().split('.').last()
				switch @type
					when 'ppt' then @type = 'powerpoint'
					when 'octet-stream' then @type = @name().split('.').last()
					when @name() then @type = 'inconnu'


		getType : () -> ko.computed(
			() -> 
				extension = @name().split('.').last()
				imagesType = ['png', 'jpg', 'jpeg', 'gif', 'ico']
				documentType = ['pdf', 'odt', 'doc', 'docx']
				if extension in imagesType then return 'image'
				if extension in documentType then return 'document'
				return 'fichier'
			this
		)

		getSprite : () -> ko.computed(
			()->
				type = @getType()()
				extension = @name().split('.').last()
				documentType = ['pdf', 'odt', 'doc', 'docx']
				return "sprite sprite-#{type}"
				###if extension in documentType
					return "filetype #{extension}"
				if type == 'image'
					return 'glyphicon glyphicon-picture'
				if type == 'fichier'
					return 'glyphicon glyphicon-file'###
			this
		)


	class Folder extends Entity
		constructor: (entity, parent) ->
			super entity, parent
			@children = ko.observableArray([])
			@setChildren entity.children
			@type = 'dossier'
			@share = ko.observable 0

		addChildJSON: (json) =>
			if json.is_directory 
				folder = new Folder json, this
				@children.push folder
				global.Folders.push folder
				return folder
			else
				file = new File json, this
				@children.push file
				global.Files.push file
				if file.type == 'image'
					global.rootContext.images.push file
				return file

		setChildren: (children) =>
			if children?
				for child in children
					@addChildJSON child


	class Link
		constructor: (jsonShare) ->
			@id = jsonShare.id
			@uniq_id = jsonShare.uniq_id
			@owner = jsonShare.user
			@file = null
			if jsonShare.files[0] isnt undefined
				for file in global.Files
					if file.id == jsonShare.files[0].id
						@file = file
						break
				if not @file?
					for folder in global.Folders
						if folder.id == jsonShare.files[0].id
							@file = folder
							break
			if @file?
				@file.link @ 

	class Share
		constructor: (jsonShare) ->
			@id = jsonShare.id
			@uniq_id = jsonShare.uniq_id
			@owner = jsonShare.user
			@folder = null
			@with = jsonShare.shared_user.email
			@mine = false
			console.log jsonShare
			if jsonShare.files[0] isnt undefined
				for folder in global.Folders
					if folder.id == jsonShare.files[0].id
						@mine = true
						@folder = folder
						break
				if @folder?
					@folder.share @
			if not @folder?

				@folder =
					name : jsonShare.files[0].name

	class sortParam
		constructor: (@property, @display) ->


	class Page
		constructor: (@name, @display) ->


	class Notification
		constructor: (@message, @level) ->
			@id = new Date().getTime()
			if not @level
				throw "Notification : notification level can't be null"

		displayClasses : () ->
			return 'alert alert-' + @level		


	class AppViewModel
		constructor: () -> 
			rootFolderEntity =
				id : 0
				name : 'Cubbyhole'
				updated_at : new Date().getTime()
			@root = new Folder rootFolderEntity
			@chosenFolder = ko.observable @root
			@selectedEntities = ko.observableArray()
			@breadcrumb = ko.observableArray()
			@query = ko.observable('')
			@isSearching = ko.observable(false)
			@isCopying = ko.observable null
			@notifications = ko.observableArray()
			@images = ko.observableArray()
			@links = 
				list : ko.observableArray()
				current : ko.observable null
			@shares = 
				list : ko.observableArray()
				current : ko.observable null


			@currentTransfer = ko.observable()
			@transferList = ko.observableArray()

			@sort =
				order : ko.observable(0)
				parameters : [
					new sortParam 'name', 'Nom'
					new sortParam 'type', 'Type'
					new sortParam 'size', 'Taille'
					new sortParam 'updated_at', 'Modifié'
				]
				chosenParam : ko.observable()

			@pages = [
				new Page 'files', 'Fichiers'
				new Page 'photos', 'Photos'
				new Page 'shares', 'Partages'
				new Page 'links', 'Liens'
			]
			@currentPage = ko.observable @pages[0]

			@changePage = (page) =>
				if @.currentPage() != page
					@.currentPage page
					body = document.getElementsByTagName('body')[0]
					body.removeAttribute 'class'
					body.classList.add @.currentPage().name
					$('img.lazy').lazyload()

			@files = ko.computed(
				()->
					search = @query().toLowerCase()
					if global?
						return ko.utils.arrayFilter global.Files, (file) ->
							return file.name().toLowerCase().indexOf(search) >= 0
				this
			)


			@formatted =
				orderClasses : ko.computed(
					()-> 
						if @sort.order() % 2 == 0
							return 'sprite sprite-arrow-up-gray'
						else 
							return 'sprite sprite-arrow-down-gray'
					this
				)		

			@changeFolder = (entity) =>
				if entity.constructor.name == 'Folder' and entity.id != null
					@chosenFolder entity
					for entity in @selectedEntities()
						entity.isSelected false
					@selectedEntities.removeAll()
					@sortEntities()
					$('img.lazy').lazyload()


			@changeSortParam = (sortParam) =>
				if sortParam == @sort.chosenParam()
					@sort.order @sort.order() + 1
				else
					@sort.chosenParam sortParam
					@sort.order 0
				@sortEntities()

			@search = (value) =>
				@files.removeAll()
				for file in global.Files
					if file.name().toLowerCase().indexOf value.toLowerCase() >= 0
						@files.push files[i]


			@appendNotifications = (message, level) =>
				notification = new Notification message, level
				@notifications.push notification
				setTimeout(
					()=>
						if notification?
							@notifications.remove notification
					5000
				)


			
			@sort.chosenParam @sort.parameters[0]
			@changeFolder @root
			@breadcrumb.push @root


		updateFilesList : (cb) ->
			$.getJSON '/app/files', (entities, status, jqxhr) =>
				@root.setChildren entities
				@sortEntities()
				if cb?
					if typeof cb == 'function'
						cb()
	
		isNameUsed : (name, folder) ->
			folder = if folder? then folder else @chosenFolder()
			entities = folder.children()
			used = false
			for entity in entities
				if name == entity.name()
					used = true
					return used
			return used
		
		sortEntities : () =>
			@chosenFolder().children.sort(
				(a, b) =>
					if a.constructor.name > b.constructor.name 
						return -1 
					else if a.constructor.name < b.constructor.name
						return 1

					aProperty = a[@sort.chosenParam().property]
					bProperty = b[@sort.chosenParam().property]
					if ko.isObservable aProperty
						aProperty = aProperty()
						bProperty = bProperty()
					if aProperty < bProperty
						if @sort.order() % 2 == 0 then return (-1) else return 1
					else if aProperty > bProperty
						if @sort.order() % 2 == 0 then return 1 else return (-1)
					else
						if (a.name() < b.name())
							if @sort.order() % 2 == 0 then return (-1) else return 1
						else
							if @sort.order() % 2 == 0 then return 1 else return (-1)
			)


	actions =
		createFolder : (parent, name, callback) ->
			if not parent?
				throw 'Folder creation failed, no parent'
			if not name?
				throw 'Folder create failed, no name defined'
			$.getJSON "/app/createFolder/#{parent.id}/#{name}", (entity) =>
		    	if entity.id
		    		rc = global.rootContext
		    		folder = parent.addChildJSON entity
		    		rc.sortEntities()
		    	if callback?
		    		callback(folder)

		download : (entity)->
			constructor = entity.constructor.name
			if constructor == 'File'
				a = document.createElement 'a'
				a.setAttribute 'href', "/download/#{entity.uniq_id}/#{entity.name()}?d=1"
				hiddenDiv = document.getElementById 'hidden'
				hiddenDiv.appendChild a
				a.click()
				hiddenDiv.removeChild a
			else
				rc = global.rootContext
				rc.appendNotifications 'Téléchargement de dossier à implémenter', 'danger'				

		remove : (entity, cb)->
			removeFolder = (folder) ->
				for child in folder.children()
					if child.constructor.name is 'Folder'
						removeFolder child
					else if child.constructor.name is 'File'
						index = global.Files.indexOf child
						if index > -1
							global.Files.splice index,1
						if child.type == 'image'
							rc = global.rootContext
							rc.images.remove child

			$.getJSON "/app/deleteFile/#{entity.id}", (response) ->
				if response.status == "ok"
					rc = global.rootContext
					rc.chosenFolder().children.remove entity
					rc.selectedEntities.remove entity
					type = entity.constructor.name
					array = global[type + 's']
					index = array.indexOf entity
					if index >= 0						
						array.splice index, 1
					if cb
						cb true
					if type is 'File'
						if entity.type == 'image'
							images = global.rootContext.images
							images.remove entity
					else if type is 'Folder'
						removeFolder entity
				else
					if cb
						cb false
					

		rename : (entity, name)->
			if not name
				entity.previousName entity.name()
				input = document.querySelector 'input.entity-name.rename'
				input.value = entity.name()
				$(input).focus()
			else
				rc = global.rootContext
				error = rc.isNameUsed name
				if not error
					#/app/renameFile/{fileId}/{newName}
					$.getJSON "/app/renameFile/#{entity.id}/#{name}", (response) ->
						if response.name == name
							entity.name name
							entity.previousName null
						else
							entity.previousName null
							rc.appendNotifications "Erreur inconnu lors du renommage du fichier #{entity.name()}.", "danger"
				else
					entity.previousName null
					if name != entity.name()
						rc.appendNotifications "Erreur lors du renommage du fichier #{entity.name()}, un fichier portant ce nom existe déjà.", "danger"

		move : (dropFolder, entities) ->
			rc = global.rootContext
			if dropFolder in entities
				rc.appendNotifications 'Impossible de déplacer un dossier dans lui même', 'danger'
			else
				for entity in entities
					((entity) ->
						error = rc.isNameUsed entity.name(), dropFolder
						if not error
							try
								$.getJSON "/app/moveFile/#{entity.id}/#{dropFolder.id}", (response, status, jqxhr) =>
									if response.status == 'ok'
										rc.chosenFolder().children.remove entity
										dropFolder.children.push entity
										entity.parent = dropFolder
										entity.isSelected false
									else if response == 'false'
										rc.appendNotifications "Erreur lors du déplacement de #{entity.name()}", 'danger'
							catch e
								rc.appendNotifications "Erreur lors du déplacement de #{entity.name()}", 'danger'
						else
							rc.appendNotifications "Erreur lors du déplacement de #{entity.name()}, un fichier portant ce nom existe déjà.", 'danger'
					).call(this, entity)			
				rc.selectedEntities.removeAll()

		copy : (dropFolder, entity, callback) ->
			rc = global.rootContext
			queue = []
			count = 1
			countFolderChildren = (entity) ->
				for child in entity.children()
					if child.constructor.name is 'Folder'
						++count
						countFolderChildren child

			copyFiles = (queue, index, callback) ->
				if index < queue.length
					file = queue[index][0]
					rc.isCopying file
					dropFolder = queue[index][1]
					$.getJSON "/app/copyFile/#{file.id}/#{dropFolder.id}", (response, status, jqxhr) ->
						dropFolder.addChildJSON response
						$('img.lazy').lazyload()
						copyFiles queue, ++index, callback
				else
					rc.isCopying null
					rc.appendNotifications "Copie de #{entity.name()} terminé", 'success'
					if callback?
						callback()

			copyFolder = (dropFolder, entity, callback) ->
				if entity.constructor.name is 'Folder'
					actions.createFolder dropFolder, entity.name(), (folder) ->
						--count
						for child in entity.children()
							if child.constructor.name is 'Folder'
								copyFolder folder, child, callback
							else
								queue.push [child, folder]
						if count == 0
							copyFiles queue, 0, callback

			if entity.constructor.name is 'Folder'
				error = false
				parentFolder = dropFolder
				while parentFolder?
					if parentFolder.id == entity.id
						error = "Impossible de copier un dossier à l'intérieur de lui même."
						parentFolder = null
					else
						parentFolder = parentFolder.parent
				if not error
					for child in dropFolder.children()
						if child.name == entity.name
							error = "Un fichier portant ce nom existe déja"
							break
				if error
					rc.appendNotifications error, "danger"
				else
					countFolderChildren(entity)
					copyFolder(dropFolder, entity, callback)
			else if entity.constructor.name is 'File'
				rc.isCopying entity
				$.getJSON "/app/copyFile/#{entity.id}/#{dropFolder.id}", (response, status, jqxhr) ->
					dropFolder.addChildJSON response
					rc.isCopying null
					$('img.lazy').lazyload()
					if callback?
						callback()

		copyMultiple : (dropFolder, entities, index) ->
			if not index? then index = 0
			console.log entities[index]
			if index < entities.length
				actions.copy(dropFolder, entities[index], -> actions.copyMultiple dropFolder, entities, ++index)


		share_link : (entity)->
			if entity.link() == 0
				req = new XMLHttpRequest()
				formData = new FormData()
				formData.append 'files', JSON.stringify [entity.id]
				formData.append 'writable', 0
				console.log JSON.stringify [entity.id]
				req.open 'POST', "app/share/null"
				req.onreadystatechange = ->
					if req.readyState is 4
						if req.status is 200
							jsonLink = JSON.parse req.responseText
							console.log jsonLink
							a = document.createElement 'a'
							a.href = "/shareLink/#{jsonLink.uniq_id}"
							a.setAttribute 'target', '_blank'
							a.style.display = 'none'
							body = document.querySelector 'body'
							body.appendChild a
							a.click()
							body.removeChild a			
				req.send formData

		share_folder : (entity)->
			if entity.constructor.name is 'Folder'
				if entity.share() is 0
					rc = global.rootContext
					rc.shares.current entity
					$('#modal-share').modal()


	global = 
		rootContext : new AppViewModel()
		Files : []
		Folders : []
		router : null
	ko.applyBindings global.rootContext

	global.rootContext.updateFilesList () ->
		getLinks = (callback) ->
			rc = global.rootContext			
			$.getJSON '/app/listShare', (response) ->
				if response.sharedToOther?
					for jsonShare in response.sharedToOther
						if jsonShare.writable is false
							link = new Link jsonShare
							if link.file?
								here = false
								for existingLink in rc.links.list()
									if existingLink.id == link.id 
										here = true
										break
								if not here
									rc.links.list.push link
							else
								$.getJSON "/app/deleteShare/#{link.id}"
				if callback?
					callback()

		getShares = (callback) ->
			console.log 'shares'
			rc = global.rootContext			
			$.getJSON '/app/listShare', (response) ->
				if response.sharedToOther?
					rc.shares.list.removeAll()
					for jsonShare in response.sharedToOther
						if jsonShare.writable is true
							if jsonShare.files[0] isnt undefined
								share = new Share jsonShare
								rc.shares.list.push share
				if response.sharedToMe?
					for jsonShare in response.sharedToMe
						if jsonShare.files[0] isnt undefined
							share = new Share jsonShare
							console.log share
							rc.shares.list.push share
			if callback?
				callback()


		global.router = Sammy(()->
			rc = global.rootContext

			@.get '#/photos', () ->
				rc.changePage rc.pages[1]
			
			@.get '#/shares', () ->
				rc.changePage rc.pages[2]
				getShares()

			@.get '#/links', () ->
				rc.changePage rc.pages[3]
				getLinks () ->
					$('img.lazy').lazyload()


			@.get /\#\/files(\/(.*)){0,1}/, () ->
				rc.changePage rc.pages[0]
				path = @.params.splat[0]
				pathArr = path.split '/'
				if not pathArr.last()
					pathArr.pop()
				if not pathArr[0]
					pathArr.shift()
				rootFolder = rc.root
				currentFolder = rootFolder
				folders = []
				found = false
				if pathArr.length > 0
					for folderName in pathArr
						for child in currentFolder.children()
							if child.constructor.name == 'Folder'
								if child.name() == folderName
									currentFolder = child
									folders.push child
									found = true
									break
						if found is false
							return
						found = false
					rc.breadcrumb.removeAll()
					rc.breadcrumb.push rc.root
					for folder in folders
						rc.breadcrumb.push folder
					rc.changeFolder folders.last()
				else
					rc.breadcrumb.removeAll()
					rc.breadcrumb.push rc.root
					rc.changeFolder rc.root

		).run()

		getLinks () ->
			body = document.getElementsByTagName('body')[0] 
			body.classList.add('files')
			body.style.height = $( window ).height() + 'px'
			pages = document.querySelectorAll 'div.page'
			for p in pages
				p.classList.remove 'page-hidden'

			global.Folders.push global.rootContext.root
			$('img.lazy').lazyload()


	$('[data-toggle="tooltip"]').tooltip()

	$('html').on 'scroll', 'body.modal-open', (event) ->
		event.preventDefault()
	

	window.onresize = (event) ->
		body = document.getElementsByTagName('body')[0] 
		body.style.height = $( window ).height() + 'px'
		

	$('#browse-files').on 'dblclick', 'tr.browse-file', () ->
		lien = this.querySelector 'span.entity-name a'
		lien.click()


				

	$('#context-menus').on 'click', '.context-menu', (event) ->
		this.classList.remove 'hidden'
		this.classList.add 'hidden'


	/// BLOCK FANCY BOX ///
	$('body').on 'contextmenu', 'div.fancybox-type-image', (event) ->
		positionX = event.clientX
		positionY = event.clientY
		menu = document.getElementById 'fancybox-context-menu'
		menu.classList.remove 'hidden'
		menu.style.position = 'absolute'
		menu.style.top = positionY + 'px'
		menu.style.left = positionX + 'px'
		imageSrc = document.querySelector('.fancybox-wrap .fancybox-inner img.fancybox-image').getAttribute 'src'
		imageName = imageSrc.split('/').last()
		downloadLink = menu.querySelector '#fancybox-download a'
		displayLink = menu.querySelector '#fancybox-display-original a'
		downloadLink.href = imageSrc + '?d=1'
		downloadLink.setAttribute 'download', imageName
		displayLink.href = imageSrc
		event.preventDefault()
		
	$('body').on 'click', '.fancybox-overlay', ()->
		menu = document.getElementById 'fancybox-context-menu'
		menu.classList.remove 'hidden'
		menu.classList.add 'hidden'



	triggerFancybox = (a) ->
		if 'fancybox' in a.classList
			links = document.querySelectorAll '.current-page a.fancybox'
			linksArray = []
			boxElements = []
			for link in links
				boxElements.push(
					{
						href : link.href
						time : link.textContent
					}
				)
				linksArray.push(link)
			start = linksArray.indexOf a
			$.fancybox.open(
				boxElements
				{
					padding : 0
					margin : 20
					index : start
					autoCenter : true
					beforeClose : () -> 
						menu = document.getElementById 'fancybox-context-menu'
						menu.classList.remove 'hidden'
						menu.classList.add 'hidden'		
				}
			)
		else
			throw "fancybox : le lien n'est pas une image"

	/// END BLOCK ///



	/// BLOCK FILES UPLOAD///
	chooseFilesHandler = (() ->
		inputFilesBtn = document.getElementById 'input-files'
		$('#modals').on 'click', '#chooseFileBtn', () ->
			inputFilesBtn.click()
	).call(this)

	onChangeInputFiles = (() ->
		inputFilesBtn = document.getElementById 'input-files'
		inputFilesBtn.addEventListener "change", () ->
			filesHandler @files
	).call(this)

	filesHandler = (files, parent) ->
		uploadProgressHandler = (e) ->
			if e.lengthComputable
				percentComplete = Math.round(e.loaded * 100 / e.total);
				console.log percentComplete
				percentBar = document.querySelector '#transferProgress .progress-bar'
				percentBar.style.width = percentComplete + '%'

		uploadFailedHandler = (e) ->
			rc.appendNotifications "Le transfert de dossier n'est pas supporté.", "danger"
			global.rootContext.currentTransfer null

		sendFiles = (files, index, parent) ->
			uri = "/upload"
			xhr = new XMLHttpRequest()
			xhr.upload.addEventListener 'progress', uploadProgressHandler, false
			xhr.addEventListener 'error', uploadFailedHandler, false


			fd = new FormData()
			rc = global.rootContext
			parent = if parent? then parent else rc.chosenFolder()

			xhr.open "POST", uri, true

			xhr.onreadystatechange = () ->
				if xhr.readyState == 4 and xhr.status == 200
					entity = JSON.parse xhr.responseText
					parent.addChildJSON entity	
					++index
					$('img.lazy').lazyload()
					global.rootContext.currentTransfer null
					if index < files.length
						sendFiles files, index, parent	

			fd.append 'parent', parent.id
			fd.append 'myFile', files[index]
			global.rootContext.currentTransfer files[index]
			if files[index].size is 0
				rc.appendNotifications "Le transfert de dossier n'est pas supporté. Impossible de transférer #{files[index].name}", "danger"
				global.rootContext.currentTransfer null
				if ++index < files.length
					sendFiles files, index, parent
			else
				reader = new FileReader()
				reader.addEventListener 'load', ->
					xhr.send fd
				try
					reader.readAsText(files[index].slice(0, 512))
				catch error
					rc.appendNotifications "Le transfert de dossier n'est pas supporté. Impossible de transférer #{files[index].name}", "danger"
					global.rootContext.currentTransfer null
					if ++index < files.length
						sendFiles files, index, parent


		rc = global.rootContext
		parent = if parent? then parent else rc.chosenFolder()
		count = 0
		sendFiles files, 0, parent




	/// END BLOCK ///


	/// BLOCK SINGLE ENTITY EVENT ///


	ActionsBarEventHandler = (() ->
		rc = global.rootContext

		expandTreeBranch = (e) ->
			e.stopPropagation()
			parent = this.parentNode.parentNode
			folderId = parseInt parent.getAttribute 'data-folder-id'
			for f in global.Folders
				if f.id == folderId
					folder = f
					break
			if folder?
				entities = folder.children()
				html = document.createElement 'div'
				html.classList.add 'branch'
				for entity in entities
					row = treeBranch entity
					if row
						row.style.marginLeft = '16px'
						html.appendChild row

				parent.appendChild html
				this.removeEventListener 'click', expandTreeBranch, true
				this.classList.remove 'glyphicon-plus'
				this.classList.add 'glyphicon-minus'
				this.addEventListener 'click', hideTreeBranch, true
			return null

		hideTreeBranch = (e) -> 
			e.stopPropagation()
			parent = this.parentNode.parentNode
			branch = parent.querySelector '.branch'
			$(branch).hide()
			this.classList.remove 'glyphicon-minus'
			this.classList.add 'glyphicon-plus'
			this.removeEventListener 'click', hideTreeBranch, true
			this.addEventListener 'click', showTreeBranch, true

		showTreeBranch = (e) -> 
			e.stopPropagation()
			parent = this.parentNode.parentNode
			branch = parent.querySelector '.branch'
			$(branch).show()
			this.classList.remove 'glyphicon-plus'
			this.classList.add 'glyphicon-minus'
			this.removeEventListener 'click', showTreeBranch, true
			this.addEventListener 'click', hideTreeBranch, true

		treeBranch = (entity) ->
			if entity.constructor.name == 'Folder'
				row = document.createElement 'div'
				row.setAttribute 'data-folder-id', entity.id
				folder = document.createElement 'div'
				folder.setAttribute 'data-folder-id', entity.id
				folder.classList.add 'folder'
				folder.addEventListener 'click', ()-> 
					folders = document.querySelectorAll 'div.folder-tree div.folder.selected'
					for f in folders
						f.classList.remove 'selected'
					folder.classList.add 'selected'
				false
				name = document.createElement 'span'
				name.textContent = entity.name()
				name.classList.add 'folder-name'
				name.addEventListener 'click', 
				icon = document.createElement 'span'
				icon.classList.add 'glyphicon'
				icon.classList.add 'glyphicon-folder-close'
				row.appendChild folder
				folder.appendChild icon
				folder.appendChild name
				if entity.children().length > 0
					for child in entity.children()
						if child.constructor.name == 'Folder'
							plus = document.createElement 'span'
							plus.classList.add 'action'
							plus.classList.add 'glyphicon'
							plus.classList.add 'glyphicon-plus'
							plus.addEventListener 'click', expandTreeBranch, true
							folder.insertBefore plus, icon
							break
			return row


		$('#modals').on 'click', (event) ->
			event.stopPropagation()
			event.preventDefault()


		$('#modals').on 'click', 'button#remove-single-btn', () ->
			$('#modal-action-remove-single').modal('hide')
			entity = rc.selectedEntities()[0]
			actions.remove entity, (response) ->
				if response is false
					rc.appendNotifications 'Erreur dans la supression de ' + entity.name(), 'danger'
			

		$('#modals').on 'click', 'button#remove-multiple-btn', () ->
			$('#modal-action-remove-multiple').modal('hide')
			finished = 0
			error = 0;
			for entity in rc.selectedEntities()
				actions.remove entity, (response) ->
					if response is false
						++error
						rc.appendNotifications 'Erreur dans la supression de ' + entity.name(), 'danger'
					if error == 1
							rc.appendNotifications "#{error} élément n'a pas été suprimé", 'danger'
					else if error > 1
						rc.appendNotifications "#{error} éléments n'ont pas été suprimé", 'danger'


		$('#modals').on 'click', 'button#move-btn', () ->
			selectedRow = document.querySelector('div.folder-tree.move div.folder.selected')
			if selectedRow?
				folderId = parseInt selectedRow.getAttribute 'data-folder-id' 
				for f in global.Folders
					if f.id == folderId
						dropFolder = f
						break
				if dropFolder?
					rc = global.rootContext
					actions.move dropFolder, rc.selectedEntities()
			$('#modal-action-move').modal('hide')


		$('#modals').on 'click', 'button#copy-btn', ->
			selectedRow = document.querySelector('div.folder-tree.copy div.folder.selected')
			if selectedRow?
				folderId = parseInt selectedRow.getAttribute 'data-folder-id' 
				for f in global.Folders
					if f.id == folderId
						dropFolder = f
						break
				if dropFolder?
					rc = global.rootContext
					if rc.selectedEntities().length is 1
						actions.copy dropFolder, rc.selectedEntities()[0]
					else if rc.selectedEntities().length > 1
						actions.copyMultiple dropFolder, rc.selectedEntities()
						
			$('#modal-action-copy').modal('hide')	



		$('#browse-files').on 'click', 'tr.actions-bar span.action', (event) ->
			dropdown = document.querySelector '.actions-bar .dropdown.open'
			if dropdown?
				dropdown.classList.remove 'open'

		$('#browse-files').on 'click', 'tr#actions-bar-multiple span#remove-multiple-modal', (event) ->
			$('#modal-action-remove-multiple').modal()


		$('#browse-files').on 'click', 'tr.actions-bar span.move-modal', (event) ->
			$('#modal-action-move').modal()
			html = document.createElement 'div'
			row = treeBranch rc.root
			if row? then html.appendChild row
			folderTree = document.querySelector '.folder-tree.move'
			while folderTree.firstChild
				folderTree.removeChild folderTree.firstChild
			folderTree.appendChild html

		$('#browse-files').on 'click', 'tr.actions-bar span.copy-modal', (event) ->
			$('#modal-action-copy').modal()
			entities = rc.root.children()
			html = document.createElement 'div'
			row = treeBranch rc.root
			if row? then html.appendChild row
			folderTree = document.querySelector '.folder-tree.copy'
			while folderTree.firstChild
				folderTree.removeChild folderTree.firstChild
			folderTree.appendChild html


		$("#browse-files").on 'click', 'tr#actions-bar-single span.action', (event) ->
			id = this.getAttribute('id')
			if id?					
				action = id.split('-')[0]
				if action == 'remove'
					$('#modal-action-remove-single').modal()
				else
					entity = rc.selectedEntities()[0]
					actions[action](entity)
			else
				classes = this.classList
				if 'dropdown' in classes
					if 'open' in classes
						classes.remove 'open'
					else
						classes.add 'open'

					
		$("#browse-files").on 'click', 'tr.browse-file span.entity-name', (event) ->
			rc = global.rootContext
			breadcrumb = rc.breadcrumb
			context = ko.contextFor this
			if context?
				entity = context.$data
				if entity.constructor.name == 'File'
					if entity.type == 'image'
						a = this.getElementsByTagName('a')[0]
						triggerFancybox a
						event.preventDefault()
			event.stopPropagation()

		enterPressed = false
		$('#browse-files').on 'keypress', 'tr input.entity-name.rename', (event) ->
			if event.charCode == 13
				enterPressed = true
				context = ko.contextFor this
				entity = context.$data
				if this.value
					actions.rename entity, this.value
				else
					entity.previousName null
				enterPressed = false

		$("#browse-files").on 'focusout', 'tr input.entity-name.rename', (event) ->
			if not enterPressed
				context = ko.contextFor this
				entity = context.$data
				if this.value
					actions.rename entity, this.value
				else
					entity.previousName null
	).call(this)

	/// ENDBLOCK ///


	/// BLOCK ENTITY SELECTION ///
	entitySelectionHandler = (() ->
		rc = global.rootContext
		shiftKeyActive = false

		document.addEventListener 'keydown', (event) -> 
			if event.shiftKey
				shiftKeyActive = true
		false

		document.addEventListener 'keyup', (event) -> 
			shiftKeyActive = false
		false

		
		$('#browse-files').on 'dragstart', 'tr.browse-file', (event) ->
			if rc.selectedEntities().length == 0
				$(this).click()

		firstSelected = null
		$('#browse-files').on 'click', 'tr.browse-file', (event) ->
			context = ko.contextFor this
			selectedEntity = context.$data
			parent = rc.chosenFolder()
			if rc.isSearching() is false then entities = parent.children() else entities = rc.files()
			if not shiftKeyActive
				rc.selectedEntities.removeAll()
				rc.selectedEntities.push selectedEntity
				firstSelected = selectedEntity
			else
				if not selectedEntity.isSelected()
					if firstSelected?
						start = entities.indexOf firstSelected
						end = entities.indexOf selectedEntity
						rc.selectedEntities.removeAll()
						for i in [start..end]
							rc.selectedEntities.push entities[i]
					else
						rc.selectedEntities.removeAll()
						rc.selectedEntities.push selectedEntity
						firstSelected = selectedEntity
				else if selectedEntity.isSelected()
					rc.selectedEntities.remove selectedEntity
					firstSelected = rc.selectedEntities()[0]

			for entity in entities
				if entity in rc.selectedEntities()
					entity.isSelected true
				else
					entity.isSelected false
			menu = document.getElementById 'browser-context-menu'
			menu.classList.remove 'hidden'
			menu.classList.add 'hidden'
			event.stopPropagation()


		$('html').on 'click', 'body.files', () ->
			rc = global.rootContext
			if firstSelected?
				firstSelected = null
			for entity in rc.selectedEntities()
				entity.isSelected false
			rc.selectedEntities.removeAll()
			menu = document.getElementById 'browser-context-menu'
			menu.classList.remove 'hidden'
			menu.classList.add 'hidden'


		$("#browse-files").on 'click', 'tr.actions-bar span.action', (event) ->
			event.stopPropagation()
	).call(this)
	/// END BLOCK ///


	/// BLOCK FOLDER ///
	folderHander = (() ->
		newFolderBtn = document.getElementById('action-new-folder')
		rc = global.rootContext

		createTmpFolder = () ->
			parent = rc.chosenFolder()
			FolderEntity =
				id : null
				name : null
				updated_at : new Date().getTime()
			folder = new Folder FolderEntity, parent
			parent.children.unshift folder
			rc.selectedEntities.removeAll()
			rc.selectedEntities.push folder
			document.querySelector('#browse-files input.entity-name.create-folder').focus()

		validateTmpFolder = (name) ->
			parent = rc.chosenFolder()
			for entity in parent.children()
				if entity.constructor.name == 'Folder'
					if not entity.name()
						folder = entity
						break
			if name
				if not rc.isNameUsed name
				    folder.name 'Création du dossier'
				    actions.createFolder parent, name, () ->
				    	parent.children.remove folder
				else
					rc.appendNotifications 'Impossible de créer ce dossier. Un dossier avec ce nom existe déja', 'warning'
					parent.children.remove folder
					rc.selectedEntities.removeAll()
			else
				rc.appendNotifications 'Impossible de créer un dossier sans nom.', 'warning'
				parent.children.remove folder
				rc.selectedEntities.removeAll()


		enterPressed = false		
		$('#browse-files').on 'keypress', 'tr.folder input.entity-name.create-folder', (event) ->
			code = event.charCode || event.keyCode
			if code == 13 
				enterPressed = true
				validateTmpFolder @.value
				enterPressed = false

		$('#browse-files').on 'focusout', 'tr.folder input.entity-name.create-folder', (event) ->
			if not enterPressed
				validateTmpFolder this.value

		newFolderBtn.addEventListener 'click', createTmpFolder, false
	).call(this)
	/// END BLOCK ///


	/// BLOCK DRAG & DROP ///
	filesDragAndDropHander = (() ->
		$('html').on 'drop', 'body.files', (event) ->
			event.stopPropagation()
			event.preventDefault()
			oe = event.originalEvent
			dt = oe.dataTransfer
			files = dt.files
			if files.length
				filesHandler files

		$('html').on 'dragenter', 'body.files', (event) ->
			event.stopPropagation()
			event.preventDefault()

		$('html').on 'dragover', 'body.files', (event) ->
			event.stopPropagation()
			event.preventDefault()

		$('body').on 'drop', 'div.fancybox-overlay', (event) ->
			event.stopPropagation()
			event.preventDefault()

		$('body').on 'dragstart', 'img.fancybox-image', (event) ->
			event.stopPropagation()
			event.preventDefault()
	).call(this)

	EntitiesDragAndDropHandler = (() ->
		dragstart = false
		$('table#browse-files').on 'dragstart', 'tbody tr', (event) ->
			try
				event.originalEvent.dataTransfer.setData('text/plain', ''); #firefox fix
			catch e 
				null
			dragstart = true
			context = ko.contextFor event.currentTarget
			entity = context.$data
			rc = global.rootContext
			if rc.selectedEntities().length == 0
				rc.selectedEntities.push entity
			else if rc.selectedEntities().length == 1
				rc.selectedEntities.removeAll()
				rc.selectedEntities.push entity
			else if rc.selectedEntities().length > 1
				if not entity.isSelected()
					rc.selectedEntities.removeAll()
					rc.selectedEntities.push entity


			for entity in rc.chosenFolder().children()
				if entity in rc.selectedEntities()
					entity.isSelected true
				else
					entity.isSelected false



		$('table#browse-files').on 'dragover', 'tbody tr.folder', (event) ->
			event.stopPropagation()
			event.preventDefault()

		$('table#browse-files').on 'dragenter', 'tbody tr.folder', (event) ->
			event.stopPropagation()
			event.preventDefault()


		$('table#browse-files').on 'drop', 'tbody tr.folder', (event) ->
			event.stopPropagation()
			event.preventDefault()
			files = event.originalEvent.dataTransfer.files
			context = ko.contextFor event.currentTarget
			if context?
				dropFolder = context.$data
				if files.length
					filesHandler files, dropFolder
				else
					rc = global.rootContext
					actions.move dropFolder, rc.selectedEntities()
				

		$('#breadcrumb').on 'drop', 'a', (event) ->
			event.stopPropagation()
			event.preventDefault()
			files = event.originalEvent.dataTransfer.files
			context = ko.contextFor event.currentTarget
			if context?
				dropFolder = context.$data
				if files.length
					filesHandler files, dropFolder
				else
					rc = global.rootContext
					actions.move dropFolder, rc.selectedEntities()

	).call(this)
	

	///END BLOCK ///


	/// BLOCK SEARCH ///
	$('#browse-search-input').on 'keyup', () ->
		rc = global.rootContext
		if @value.length > 0
			rc.isSearching true
		else
			rc.isSearching false
			for entity in rc.selectedEntities()
				entity.isSelected false
			rc.selectedEntities.removeAll()

	/// END BLOCK ///


	///BLOCK CONTEXT MENU BROWSE FILE ///
	$('#browse-files').on 'contextmenu', 'tr.browse-file', (event) ->
		rc = global.rootContext
		event.preventDefault()
		context = ko.contextFor this
		entity = context.$data
		if entity?
			if entity not in rc.selectedEntities()
				this.click()			
			positionX = event.clientX
			positionY = event.clientY
			menu = document.getElementById 'browser-context-menu'
			menu.classList.remove 'hidden'
			menu.style.position = 'fixed'
			menu.style.top = positionY + 'px'
			menu.style.left = positionX + 'px'
			if rc.selectedEntities().length is 1
				contextShareLink = menu.querySelector('#browser-context-menu-share')
				console.log contextShareLink
				console.log entity.link()
				if entity.link() is 0
					contextShareLink.classList.remove 'hidden'
				else
					contextShareLink.classList.add 'hidden'

	$('#browse-files').on 'click', 'tr.actions-bar span', ->
		menu = document.getElementById 'browser-context-menu'
		menu.classList.remove 'hidden'
		menu.classList.add 'hidden'

	$('#browser-context-menu').on 'click', (event) ->
		this.classList.remove 'hidden'
		this.classList.add 'hidden'
		event.stopPropagation()

	$('#browser-context-menu').on 'contextmenu', (event) ->
		event.preventDefault()

	$('#browser-context-menu-download-single').on 'click', ->
		document.querySelector('#actions-bar-single #download-single').click()

	$('#browser-context-menu-share').on 'click', ->
		document.querySelector('#actions-bar-single #share_link-single').click()

	$('#browser-context-menu-remove').on 'click', ->
		rc = global.rootContext
		if rc.selectedEntities().length == 1
			document.querySelector('#actions-bar-single #remove-single-modal').click()
		else if rc.selectedEntities().length > 1
			document.querySelector('#actions-bar-multiple #remove-multiple-modal').click()

	$('#browser-context-menu-rename').on 'click', ->
		document.querySelector('#actions-bar-single #rename-single').click()

	$('#browser-context-menu-move').on 'click', ->
		document.querySelector('.move-modal').click()


	$('#browser-context-menu-copy').on 'click', ->
		document.querySelector('.copy-modal').click()

	///END BLOCK///



	/// PAGE PHOTOS ///
	$('#photos-browser').on 'click', 'div.photo a', (event) ->
		event.preventDefault()

	$('#photos-browser').on 'dblclick', 'div.photo a', (event) ->	
		triggerFancybox this
		event.preventDefault()

	/// END PAGE ///


	/// PAGE LINKS ///
	$('#links-page').on 'click', 'div.link-delete', (event) ->
		rc = global.rootContext
		context = ko.contextFor this
		file = context.$data
		if file?
			rc.links.current file
			console.log file
			$('#modal-delete-link').modal()

	$('#delete-link-btn').on 'click', (event) ->
		$('#modal-delete-link').modal('hide')
		rc = global.rootContext
		file = rc.links.current()
		if file?
			$.getJSON "/app/deleteShare/#{file.link().id}", ->
				rc.links.current null
				for link in rc.links.list()
					if link.id == file.link().id
						rc.links.list.remove link
						break
				file.link 0


	/// END PAGE ///


	/// PAGE SHARES ///


	$('#share-btn').on 'click', (event) ->
		email = document.querySelector('#modal-share input[type="email"]').value.trim()
		if email.length > 0
			$('#modal-share').modal('hide')
			rc = global.rootContext
			req = new XMLHttpRequest()
			formData = new FormData()
			formData.append 'files', JSON.stringify [rc.shares.current().id]
			formData.append 'writable', 1
			console.log email
			req.open 'POST', "app/share/#{email}"
			req.onreadystatechange = ->
				if req.readyState is 4
					if req.status is 200
						response = JSON.parse req.responseText
						if response.status?
							if response.status is "error"
								rc.appendNotifications "Ancun utilisateur correspond à l'email spécifié", "danger"
						else
							a = document.createElement 'a'
							a.href = "/shareLink/#{response.uniq_id}"
							a.setAttribute 'target', '_blank'
							a.style.display = 'none'
							body = document.querySelector 'body'
							body.appendChild a
							a.click()
							body.removeChild a			
			req.send formData

	$('#shares-page').on 'click', 'div.share-delete', (event) ->
		rc = global.rootContext
		context = ko.contextFor this
		folder = context.$data


	/// END PAGE ///