<?php

namespace AllanBrault\ApiBundle\Controller;

use AllanBrault\CoreBundle\Entity\ChFile;
use AllanBrault\CoreBundle\Entity\ShareLink;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerBuilder;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Doctrine\ORM\Query;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ChFileController extends Controller
{
    private function checkParent($parent,$file)
    {
        $em = $this->getDoctrine()->getManager();
        $ff = $em->getRepository('CoreBundle:ChFile')->findBy(array('parent' => $parent));

        foreach($ff as $f) {
            if($f->getIsDirectory()) {
                return $this->azeaze($f,$file);
            } else {
                if($f == $file) {
                    return true;
                    break;
                }
            }
        }
        return false;
    }

    /**
     * @ApiDoc(
     *  description="download a file"
     * )
     * @Route("/download/{uniqId}/{name}", name="api.download")
     * @Method({"GET"})
     */
    public function downloadAction($uniqId,$name)
    {
        $user = $this->container->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();

        $file = $em->getRepository('CoreBundle:ChFile')->findOneBy(array('user' => $user, 'uniqId' => $uniqId, 'name' => $name));
        $sharedFile = $em->getRepository('CoreBundle:ShareLink')->findBy(array('sharedUser' => $user));

        $go = false;
        if($user == $file->getUser()) {
            $go = true;
        } else {
            foreach($sharedFile as $sfile) {
                if($sfile->getSharedUser() == $user) {
                    foreach($sfile->getFiles() as $f) {
                        if($f->getIsDirectory()) {
                            $go = $this->checkParent($f,$file);
                            if($go) { break; }
                        } else {
                            if($f == $file) {
                                $go = true;
                                break;
                            }
                        }
                    }
                }
            }
        }

        if ($file === null || !$go) {
            return new JsonResponse(array('status' => 'error','message' => 'file not found'));
        }

        $response = new BinaryFileResponse('gaufrette://storage/' . $file->getFileName());
        $response->headers->set('Content-Type', 'application/octet-stream');

        return $response;
    }

    /**
     * @ApiDoc(
     *  description="get files",
     *  output="AllanBrault\CoreBundle\Entity\ChFile"
     * )
     * @Route("/files", name="api.files")
     * @Method({"GET"})
     */
    public function getFilesAction()
    {
        $user = $this->container->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();

        $files = $em->getRepository('CoreBundle:ChFile')->createQueryBuilder('a')
            ->Where('a.parent is NULL')
            ->andWhere('a.user = :user')
            ->setParameter('user',$user)
            ->getQuery()->getResult();

        $serializer = $this->get('jms_serializer');
        $jsonContent = $serializer->serialize($files, 'json', SerializationContext::create()->enableMaxDepthChecks());

        $response = new Response($jsonContent);
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @ApiDoc(
     *  description="upload",
     *  requirements={
     *      {
     *          "name"="parent",
     *          "dataType"="integer",
     *          "description"="id of parent folder (null = root folder)"
     *      },
     *      {
     *          "name"="myFile",
     *          "dataType"="file",
     *          "description"="the file"
     *      }
     *  }
     * )
     * @Route("/upload", name="api.upload")
     * @Method({"POST"})
     */
    public function uploadAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->container->get('security.context')->getToken()->getUser();

        if ($request->files->get('myFile') != null) {
            $name = $request->files->get('myFile')->getClientOriginalName();
            $parentId = $request->get('parent');
            $parent = null;
            if($parentId != null) {
                $parent = $em->getRepository('CoreBundle:ChFile')->find($parentId);
            }

            $sameNameFile = $em->getRepository('CoreBundle:ChFile')->findOneBy(array('name' => $name,'parent' => $parent));
            if($sameNameFile != null) {
                return new JsonResponse(array('status' => 'error','message' => 'name already exist'));
            }

            $file = new ChFile();
            $file->setFile($request->files->get('myFile'));

            $file->setFileName($name);
            $file->setUser($user);
            $file->setParent($parent);

            $em->persist($file);
            $em->flush();

            $serializer = $this->get('jms_serializer');
            $jsonContent = $serializer->serialize($file, 'json');
            $response = new Response($jsonContent);
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
        return new JsonResponse(array('status'=>'error','message'=>'no file'));
    }

    /**
     * @ApiDoc(
     *  description="move file/folder to new folder, 0 = root"
     * )
     * @Route("/moveFile/{fileId}/{newFolderId}", name="api.file.move")
     * @Method({"GET"})
     */
    public function moveFileAction($fileId, $newFolderId)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->container->get('security.context')->getToken()->getUser();

        $chFile = $em->getRepository('CoreBundle:ChFile')->findOneBy(array('id' => $fileId,'user' => $user));
        $newFolder = null;
        if($newFolderId != 0) {
            $newFolder = $em->getRepository('CoreBundle:ChFile')->findOneBy(array('id' => $newFolderId,'user' => $user));
        }
        if($chFile == null) {
            return new JsonResponse(array('status'=>'error'));
        }

        $chFile->setParent($newFolder);

        $em->flush();

        return new JsonResponse(array('status'=>'ok'));
    }

    /**
     * @ApiDoc(
     *  description="rename a file/folder"
     * )
     * @Route("/renameFile/{fileId}/{newName}", name="api.file.rename")
     * @Method({"GET"})
     */
    public function renameFileAction($fileId, $newName)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->container->get('security.context')->getToken()->getUser();

        $chFile = $em->getRepository('CoreBundle:ChFile')->findOneBy(array('id' => $fileId,'user' => $user));

        if($chFile === null) {
            return new JsonResponse(array('status'=>'error'));
        }

        $chFile->setName($newName);
        $em->flush();

        $serializer = $this->get('jms_serializer');
        $jsonContent = $serializer->serialize($chFile, 'json');

        return new Response($jsonContent);
    }

    /**
     * @ApiDoc(
     *  description="delete a file/folder"
     * )
     * @Route("/deleteFile/{fileId}", requirements={"fileId" = ".+"}, name="api.file.delete")
     * @Method({"GET"})
     */
    public function deleteFileAction($fileId)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->container->get('security.context')->getToken()->getUser();

        $filesId = explode('/',$fileId);

        foreach($filesId as $fid) {
            $chFile = $em->getRepository('CoreBundle:ChFile')->findOneBy(array('id' => $fid,'user' => $user));
            if($chFile === null) {
                return new JsonResponse(array('status' => 'error','message' => $fid.' not found'));
            }
            $em->remove($chFile);
        }
        $em->flush();

        return new JsonResponse(array('status' => 'ok'));
    }

    /**
     * @ApiDoc(
     *  description="create a folder / 0 = root"
     * )
     * @Route("/createFolder/{parentId}/{name}", name="api.folder.create")
     * @Method({"GET"})
     */
    public function createFolderAction($parentId, $name)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->container->get('security.context')->getToken()->getUser();

        $folder = new ChFile();
        $folder->setUser($user);
        $folder->setIsDirectory(true);

        $folder->setName($name);

        if($parentId != 0) {
            $parent = $em->getRepository('CoreBundle:ChFile')->findOneBy(array('id' => $parentId,'user' => $user));
            if($parent == null) {
                return new JsonResponse(array('status'=>'error','message'=>'wrong parentId'));
            }
            $folder->setParent($parent);
        }

        $em->persist($folder);
        $em->flush();

        $serializer = $this->get('jms_serializer');
        $jsonContent = $serializer->serialize($folder, 'json');

        return new Response($jsonContent);
    }

    /**
     * @ApiDoc(
     *  description="copy a file"
     * )
     * @Route("/copyFile/{fileId}/{newParentId}", name="api.file.copy")
     * @Method({"GET"})
     */
    public function copyFileAction($fileId, $newParentId)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->container->get('security.context')->getToken()->getUser();

        $chFile = $em->getRepository('CoreBundle:ChFile')->findOneBy(array('id' => $fileId,'user' => $user));
        $parent = null;
        if($newParentId != 0) {
            $parent = $em->getRepository('CoreBundle:ChFile')->findOneBy(array('id' => $newParentId, 'isDirectory' => true));
        }

        if($chFile === null || $parent === null && $newParentId != 0) {
            return new JsonResponse(array('status'=>'error'));
        }

        $newFile = new ChFile();

        $path = $this->get('kernel')->getRootDir() . '/../storage/' . $chFile->getFileName();
        $newName = $em->getRepository('CoreBundle:ChFile')->getNonRepeatableName($chFile->getName(),$user,null,$newParentId);
        $newFile->setFile(new UploadedFile($path, $newName));

        $newFile->setUser($user);
        $newFile->setParent($parent);

        $em->persist($newFile);
        $em->flush();

        $serializer = $this->get('jms_serializer');
        $jsonContent = $serializer->serialize($newFile, 'json');

        return new Response($jsonContent);
    }

    /**
     * @ApiDoc(
     *  description="share multiple or a single file, to ONE user",
     *  requirements={
     *      {
     *          "name"="files",
     *          "dataType"="string",
     *          "description"="array of files/folder id shared (IN JSON) = '[12,14,15]'  pour les id 12,14 et 15"
     *      },
     *      {
     *          "name"="writable",
     *          "dataType"="bolean",
     *          "description"="is the shared user can write in the shared files/folder"
     *      }
     *  }
     * )
     * )
     * @Route("/share/{emailShared}", name="api.share.create")
     * @Method({"POST"})
     */
    public function shareFilesAction(Request $request, $emailShared)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->container->get('security.context')->getToken()->getUser();

        $shareduser = null;
        if ($emailShared != 'null') {
            $shareduser = $em->getRepository('CoreBundle:User')->findOneBy(array('email' => $emailShared));
            if($shareduser == null) {
                return new JsonResponse(array('status'=>'error','message'=>'user existe pas'));
            }
        }

        $files = json_decode($request->get('files'));
        if (!is_array($files) || count($files) > 1) {
            return new JsonResponse(array('status'=>'error','message'=>'json array'));
        }
        $writable = $request->get('writable');

        $shareLink = new ShareLink();
        $shareLink->setUser($user);
        $shareLink->setSharedUser($shareduser);
        $shareLink->setWritable($writable);

        foreach($files as $fileId) {
            $file = $em->getRepository('CoreBundle:ChFile')->find($fileId);
            $shareLink->addFile($file);
        }

        $em->persist($shareLink);
        $em->flush();

        $serializer = $this->get('jms_serializer');
        $jsonContent = $serializer->serialize($shareLink, 'json');

        return new Response($jsonContent);
    }

    /**
     * @ApiDoc(
     *  description="delete a sharelink"
     * )
     * )
     * @Route("/deleteShare/{shareId}", name="api.share.delete")
     * @Method({"GET"})
     */
    public function deleteShareAction($shareId)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->container->get('security.context')->getToken()->getUser();

        $shareLink = $em->getRepository('CoreBundle:ShareLink')->findOneBy(array('id' => $shareId,'user' => $user));
        if ($shareLink == null) {
            return new JsonResponse(array('status'=>'error'));
        }

        $em->remove($shareLink);
        $em->flush();

        return new JsonResponse(array('status'=>'ok','message'=>'deleted'));
    }

    /**
     * @ApiDoc(
     *  description="list all sharelink"
     * )
     * )
     * @Route("/listShare", name="api.share.list")
     * @Method({"GET"})
     */
    public function listShareAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->container->get('security.context')->getToken()->getUser();

        $sharedLink = $em->getRepository('CoreBundle:ShareLink')->findBy(array('sharedUser' => $user));
        $shareLink = $em->getRepository('CoreBundle:ShareLink')->findBy(array('user' => $user));
        $serializer = $this->get('jms_serializer');
        $jsonShareLink = $jsonSharedLink = null;
        if ($sharedLink != null) {
            $jsonSharedLink = $serializer->serialize($sharedLink, 'json');
        }
        if ($shareLink != null) {
            $jsonShareLink = $serializer->serialize($shareLink, 'json');
        }

        return new JsonResponse(array(
            'sharedToOther' => json_decode($jsonShareLink),
            'sharedToMe' => json_decode($jsonSharedLink)
        ));
    }
}
