<?php

namespace AllanBrault\ApiBundle\Controller;

use Doctrine\ORM\Query;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class DocController extends Controller
{
    /**
     * @Route("/doc/", name="api.doc.home")
     */
    public function docAction()
    {
        return $this->render('ApiBundle:Doc:home.html.twig');
    }

}
