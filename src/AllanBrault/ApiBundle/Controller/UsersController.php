<?php

namespace AllanBrault\ApiBundle\Controller;

use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerBuilder;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Doctrine\ORM\Query;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Validator\Constraints\Email;

class UsersController extends Controller
{
    /**
     * @ApiDoc(
     *  description="register, NO TOKEN",
     *  requirements={
     *      {
     *          "name"="form[username]",
     *          "dataType"="string",
     *          "description"="username"
     *      },
     *      {
     *          "name"="form[email]",
     *          "dataType"="string",
     *          "description"="email"
     *      },
     *      {
     *          "name"="form[plainPassword][first]",
     *          "dataType"="string",
     *          "description"="password"
     *      },
     *      {
     *          "name"="form[plainPassword][second]",
     *          "dataType"="string",
     *          "description"="password verif"
     *      }
     *  },
     *  output="AllanBrault\CoreBundle\Entity\User"
     * )
     * @Route("/register", name="api.register")
     * @Method({"POST"})
     */
    public function registerAction(Request $request)
    {
        $userManager = $this->container->get('fos_user.user_manager');

        $user = $userManager->createUser();
        $user->setEnabled(true);

        $form = $this->createFormBuilder($user, array('csrf_protection' => false))
            ->add('email', 'email', array('label' => 'form.email', 'translation_domain' => 'FOSUserBundle'))
            ->add('username', null, array('label' => 'form.username', 'translation_domain' => 'FOSUserBundle'))
            ->add('plainPassword', 'repeated', array(
                'type' => 'password',
                'options' => array('translation_domain' => 'FOSUserBundle'),
                'first_options' => array('label' => 'form.password'),
                'second_options' => array('label' => 'form.password_confirmation'),
                'invalid_message' => 'fos_user.password.mismatch',
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $userManager->updateUser($user);

            $serializer = $this->get('jms_serializer');
            $jsonContent = $serializer->serialize($user, 'json');

            return new Response($jsonContent);
        }

        return new JsonResponse(array('status' => 'error'));
    }

    /**
     * @ApiDoc(
     *  description="get profile",
     *  output="AllanBrault\CoreBundle\Entity\User"
     * )
     * @Route("/profile", name="api.profile.get")
     * @Method({"GET"})
     */
    public function getProfileAction()
    {
        $user = $this->container->get('security.context')->getToken()->getUser();

        $serializer = $this->get('jms_serializer');
        $jsonContent = $serializer->serialize($user, 'json');

        return new Response($jsonContent);
    }

    /**
     * @ApiDoc(
     *  description="set profile",
     *  requirements={
     *      {
     *          "name"="form[username]",
     *          "dataType"="string",
     *          "description"="username"
     *      },
     *      {
     *          "name"="form[email]",
     *          "dataType"="string",
     *          "description"="email"
     *      },
     *      {
     *          "name"="form[current_password]",
     *          "dataType"="string",
     *          "description"="password verif"
     *      }
     *  },
     *  output="AllanBrault\CoreBundle\Entity\User"
     * )
     * @Route("/profile", name="api.profile.set")
     * @Method({"POST"})
     */
    public function setProfileAction(Request $request)
    {
        $userManager = $this->container->get('fos_user.user_manager');

        $user = $this->container->get('security.context')->getToken()->getUser();

        $constraint = new UserPassword();

        $form = $this->createFormBuilder($user, array('csrf_protection' => false))
            ->add('username', null)
            ->add('email', 'email')
            ->add('current_password', 'password', array(
                'mapped' => false,
                'constraints' => $constraint,
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $userManager->updateUser($user);

            $serializer = $this->get('jms_serializer');
            $jsonContent = $serializer->serialize($user, 'json');

            return new Response($jsonContent);
        }

        return new JsonResponse(array('status' => 'error'));
    }

}
