<?php

namespace AllanBrault\FrontBundle\Controller;

use AllanBrault\CoreBundle\Entity\Subscription;
use DateInterval;
use DateTime;
use FOS\UserBundle\Model\UserInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("")
 */
class FrontController extends Controller {

    /**
     * @Route("/", name="front.home")
     */
    public function homeAction()
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            return $this->render('FrontBundle:Front:home.html.twig');
        }
        return $this->redirect($this->generateUrl('app.home'));
    }

    /**
     * @Route("/download/", name="front.download")
     */
    public function downloadAction()
    {
        return $this->render('FrontBundle:Front:download.html.twig');
    }

    /**
     * @Route("/about/", name="front.about")
     */
    public function aboutAction()
    {
        return $this->render('FrontBundle:Front:about.html.twig');
    }


    /**
     * @Route("/plan/{planNumber}", name="front.plan")
     */
    public function planAction(Request $request, $planNumber)
    {
        if($planNumber == 5 || $planNumber == 10) {
            return $this->render('FrontBundle:Front:plan.html.twig',array('plan' => $planNumber));
        }

        return new RedirectResponse($this->generateUrl('front.about'));
    }

}
