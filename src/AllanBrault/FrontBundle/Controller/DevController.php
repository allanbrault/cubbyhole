<?php

namespace AllanBrault\FrontBundle\Controller;

use AllanBrault\CoreBundle\Entity\ChFile;
use AllanBrault\CoreBundle\Entity\Subscription;
use DateInterval;
use DateTime;
use Doctrine\ORM\EntityRepository;
use FOS\UserBundle\Model\UserInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("")
 */
class DevController extends Controller {

    /**
     * @Route("/setplan/{planNumber}", name="front.dev.setPlan")
     */
    public function setPlanAction(Request $request, $planNumber)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        if (!is_object($user) || !$user instanceof UserInterface || $planNumber != 5 && $planNumber != 10) {
            return new RedirectResponse($this->generateUrl('front.about'));
        }

        if($this->getUser()->isSubscribe()) {
            return new RedirectResponse($this->generateUrl('fos_user_profile_show'));
        }

        $now = new DateTime();
        $in30day = clone $now;
        $in30day->add(new DateInterval('P30D'));

        $sub = new Subscription();
        $sub->setUser($user);
        $sub->setStart($now);
        $sub->setEnd($in30day);
        $sub->setSecuredArea('SUB_'.$planNumber);

        $em->persist($sub);
        $em->flush();

        return new RedirectResponse($this->generateUrl('fos_user_profile_show'));
    }
}
