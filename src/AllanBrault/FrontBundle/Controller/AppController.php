<?php

namespace AllanBrault\FrontBundle\Controller;

use AllanBrault\CoreBundle\Entity\ChFile;
use AllanBrault\CoreBundle\Entity\Subscription;
use DateInterval;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use FOS\UserBundle\Model\UserInterface;
use Imagick;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * @Route("")
 */
class AppController extends Controller {

    private function checkParent($parent,$file)
    {
        $em = $this->getDoctrine()->getManager();
        $ff = $em->getRepository('CoreBundle:ChFile')->findBy(array('parent' => $parent));

        foreach($ff as $f) {
            if($f->getIsDirectory()) {
                return $this->checkParent($f,$file);
            } else {
                if($f == $file) {
                    return true;
                    break;
                }
            }
        }
        return false;
    }

    /**
     * @Route("/home", name="app.home")
     */
    public function homeAction()
    {
        return $this->render('FrontBundle:App:home.html.twig');
    }

    /**
     * @Route("/app/files/", name="app.files")
     */
    public function filesAction()
    {
        $response = $this->forward('ApiBundle:ChFile:getFiles');
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Route("/app/profile/", name="app.profile")
     */
    public function profileAction()
    {
        $response = $this->forward('ApiBundle:Users:getProfile');
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Route("/thumb/{uniqId}/{name}", name="app.thumb")
     */
    public function thumbAction(Request $request,$uniqId,$name)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        $file = $em->getRepository('CoreBundle:ChFile')->findOneBy(array('uniqId' => $uniqId, 'name' => $name));
        $sharedFile = $em->getRepository('CoreBundle:ShareLink')->findBy(array('sharedUser' => $user));

        $go = false;
        if($user == $file->getUser()) {
            $go = true;
        } else {
            foreach($sharedFile as $sfile) {
                if($sfile->getSharedUser() == $user) {
                    foreach($sfile->getFiles() as $f) {
                        if($f->getIsDirectory()) {
                            $go = $this->checkParent($f,$file);
                            if($go) { break; }
                        } else {
                            if($f == $file) {
                                $go = true;
                                break;
                            }
                        }
                    }
                }
            }
        }
        if ($file === null || !$go) {
            return new JsonResponse(array('status' => 'error','message' => 'file not found'));
        }

        $thumb_path = $this->get('kernel')->getRootDir().'/../web/media/';
        $mime = $file->getMime();


        $thumb_size = 32;
        if($request->query->get('w') == '256') {
            $thumb_size = 256;
        }
        $thumb_fileName = $thumb_path . $thumb_size . '_' . $file->getFileName();

        if(file_exists($thumb_fileName)) {
            $response = new BinaryFileResponse($thumb_fileName);
            $response->setMaxAge(86400);
            return $response;
        }

        if($mime != 'image/png' && $mime != 'image/jpeg' && $mime != 'image/gif') {
            return new JsonResponse(array('status' => 'error','message' => 'not a image or not supported image extension'));
        }

        $im = new Imagick();
        $im->readimage('gaufrette://storage/' . $file->getFileName());
        $im->setImageFormat("png24");
        $im->setImageCompressionQuality(85);
        $im->resizeImage($thumb_size,$thumb_size,Imagick::FILTER_LANCZOS,1.1);
        $im->writeimage($thumb_fileName);
        $im->clear();
        $im->destroy();

        $response = new BinaryFileResponse($thumb_fileName);
        $response->setMaxAge(86400);
        return $response;
    }

    /**
     * @Route("/download/{uniqId}/{name}", name="app.download")
     * @Method({"GET"})
     */
    public function downloadAction(Request $request, $uniqId,$name)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        $file = $em->getRepository('CoreBundle:ChFile')->findOneBy(array('uniqId' => $uniqId, 'name' => $name));
        $sharedFile = $em->getRepository('CoreBundle:ShareLink')->findBy(array('sharedUser' => $user));

        $go = false;
        if($user == $file->getUser()) {
            $go = true;
        } else {
            foreach($sharedFile as $sfile) {
                if($sfile->getSharedUser() == $user) {
                    foreach($sfile->getFiles() as $f) {
                        if($f->getIsDirectory()) {
                            $go = $this->checkParent($f,$file);
                            if($go) { break; }
                        } else {
                            if($f == $file) {
                                $go = true;
                                break;
                            }
                        }
                    }
                }
            }
        }

        if ($file === null || !$go) {
            return new JsonResponse(array('status' => 'error','message' => 'file not found'));
        }

        $response = new BinaryFileResponse('gaufrette://storage/' . $file->getFileName());
        $response->headers->set('Content-Type', $file->getMime());
        if($request->query->get('d') == 1) {
            $response->headers->set('Content-Type', 'application/octet-stream');
            $response->setContentDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                $file->getName()
            );
        }

        return $response;
    }


    /**
     * @Route("/upload", name="app.upload")
     * @Method({"POST"})
     */
    public function uploadAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        if ($request->files->get('myFile') != null) {
            $name = $request->files->get('myFile')->getClientOriginalName();
            $parentId = $request->get('parent');
            $parent = null;
            if($parentId != null) {
                $parent = $em->getRepository('CoreBundle:ChFile')->find($parentId);
/*
                $share = $em->getRepository('CoreBundle:ShareLink')->getWritableSharedFolder($user, $parentId);



                //var_dump($share);
                die(1);
*/

                //osef
            }

            $sameNameFile = $em->getRepository('CoreBundle:ChFile')->findOneBy(array('name' => $name,'parent' => $parent));
            if($sameNameFile != null) {
                return new JsonResponse(array('status' => 'error','message' => 'name already exist'));
            }

            $file = new ChFile();
            $file->setFile($request->files->get('myFile'));

            $file->setFileName($name);
            $file->setUser($user);
            $file->setParent($parent);

            $em->persist($file);
            $em->flush();

            $serializer = $this->get('jms_serializer');
            $jsonContent = $serializer->serialize($file, 'json');

            $response = new Response($jsonContent);
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        }
        return new JsonResponse(array('status' => 'error'));
    }


    /**
     * @Route("/app/moveFile/{fileId}/{newFolderId}", name="app.file.move")
     */
    public function moveFileAction($fileId, $newFolderId)
    {
        $response = $this->forward('ApiBundle:ChFile:moveFile',array(
            'fileId' => $fileId,
            'newFolderId' => $newFolderId
        ));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Route("/app/renameFile/{fileId}/{newName}", name="app.file.rename")
     */
    public function renameFileAction($fileId, $newName)
    {
        $response = $this->forward('ApiBundle:ChFile:renameFile',array(
            'fileId' => $fileId,
            'newName' => $newName
        ));
        //$response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Route("/app/deleteFile/{fileId}", requirements={"fileId" = ".+"}, name="app.file.delete")
     */
    public function deleteFileAction($fileId)
    {
        $response = $this->forward('ApiBundle:ChFile:deleteFile',array(
            'fileId' => $fileId
        ));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Route("/app/createFolder/{parentId}/{name}", name="app.folder.create")
     */
    public function createFolderAction($parentId, $name)
    {
        $response = $this->forward('ApiBundle:ChFile:createFolder',array(
            'parentId' => $parentId,
            'name' => $name
        ));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Route("/app/copyFile/{fileId}/{newParentId}", name="app.file.copy")
     */
    public function copyFileAction($fileId, $newParentId)
    {
        $response = $this->forward('ApiBundle:ChFile:copyFile',array(
            'fileId' => $fileId,
            'newParentId' => $newParentId
        ));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Route("/app/share/{emailShared}", name="app.share.create")
     */
    public function shareFilesAction($emailShared)
    {
        $response = $this->forward('ApiBundle:ChFile:shareFiles',array(
            'emailShared' => $emailShared
        ));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Route("/app/deleteShare/{shareId}", name="app.share.delete")
     */
    public function deleteShareAction($shareId)
    {
        $response = $this->forward('ApiBundle:ChFile:deleteShare',array(
            'shareId' => $shareId
        ));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Route("/app/listShare", name="app.share.list")
     */
    public function listShareAction()
    {
        $response = $this->forward('ApiBundle:ChFile:listShare');
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Route("/shareLink/{uniqId}", name="app.share.get")
     */
    public function shareLinkAction($uniqId)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        $sharedFile = $em->getRepository('CoreBundle:ShareLink')->findOneBy(array('uniqId' => $uniqId));
        if ($sharedFile == null) {
            return new JsonResponse(array('status' => 'error','message' => 'share not found'));
        }
        if($sharedFile->getSharedUser() != null && $sharedFile->getSharedUser() != $user && $sharedFile->getUser() != $user) {
            return $this->render('FrontBundle:App:404.html.twig');
        }

        return $this->render('FrontBundle:App:share.html.twig',array('sharedFile' => $sharedFile));
    }
}
