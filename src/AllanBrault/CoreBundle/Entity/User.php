<?php

namespace AllanBrault\CoreBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;

/**
 * @ExclusionPolicy("all")
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Expose
     * @ORM\OneToMany(targetEntity="Subscription", mappedBy="user")
     */
    protected $subscriptions;

    /**
     * @ORM\OneToMany(targetEntity="ChFile", mappedBy="user")
     */
    protected $chFile;

    public function __construct()
    {
        parent::__construct();
        $this->roles[] = 'ROLE_USER';
    }

    public function getRoles()
    {
        $roles = $this->roles;

        return $roles;
    }

    public function isSubscribe()
    {
        foreach ($this->subscriptions as $subscription) {
            if (isset($subscription) && $subscription->isActive()) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param \AllanBrault\CoreBundle\Entity\Subscription $subscriptions
     * @return User
     */
    public function addSubscription(\AllanBrault\CoreBundle\Entity\Subscription $subscriptions)
    {
        $this->subscriptions[] = $subscriptions;

        return $this;
    }

    /**
     * @param \AllanBrault\CoreBundle\Entity\Subscription $subscriptions
     */
    public function removeSubscription(\AllanBrault\CoreBundle\Entity\Subscription $subscriptions)
    {
        $this->subscriptions->removeElement($subscriptions);
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubscriptions()
    {
        return $this->subscriptions;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChFile()
    {
        return $this->chFile;
    }

    /**
     * Add chFile
     *
     * @param \AllanBrault\CoreBundle\Entity\ChFile $chFile
     * @return User
     */
    public function addChFile(\AllanBrault\CoreBundle\Entity\ChFile $chFile)
    {
        $this->chFile[] = $chFile;

        return $this;
    }

    /**
     * Remove chFile
     *
     * @param \AllanBrault\CoreBundle\Entity\ChFile $chFile
     */
    public function removeChFile(\AllanBrault\CoreBundle\Entity\ChFile $chFile)
    {
        $this->chFile->removeElement($chFile);
    }
}
