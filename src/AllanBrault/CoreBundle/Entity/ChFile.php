<?php

namespace AllanBrault\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\MaxDepth;

/**
 * @ExclusionPolicy("all")
 * @Gedmo\Tree(type="nested")
 * @ORM\Table(name="chFile")
 * @ORM\Entity(repositoryClass="AllanBrault\CoreBundle\Repository\ChFileRepository")
 * @Vich\Uploadable
 */
class ChFile
{
    /**
     * @Expose
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="chFile")
     */
    protected $user;

    /**
     * @Expose
     * @ORM\Column(type="boolean", name="isDirectory")
     */
    protected $isDirectory;

    /**
     * @Expose
     * @ORM\Column(type="string", length=255, name="name", nullable=true)
     */
    protected $name;

    /**
     * @Expose
     * @ORM\Column(type="string", length=255, name="mime", nullable=true)
     */
    protected $mime;

    /**
     * @Assert\File(
     *      maxSize = "1000M"
     * )
     * @Vich\UploadableField(mapping="chFile", fileNameProperty="fileName")
     */
    protected $file;

    /**
     * @Expose
     * @ORM\Column(type="string", length=255, name="file_name", nullable=true)
     */
    protected $fileName;

    /**
     * @Expose
     * @ORM\Column(type="integer", name="file_size", nullable=true)
     */
    protected $fileSize;


    /**
     * @Expose
     * @ORM\Column(type="string", length=255, name="uniq_id", nullable=true)
     */
    protected $uniqId;

    /**
     * @Expose
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updatedAt;

    /**
     * @Gedmo\TreeLeft
     * @ORM\Column(name="lft", type="integer")
     */
    private $lft;

    /**
     * @Gedmo\TreeLevel
     * @ORM\Column(name="lvl", type="integer")
     */
    private $lvl;

    /**
     * @Gedmo\TreeRight
     * @ORM\Column(name="rgt", type="integer")
     */
    private $rgt;

    /**
     * @Gedmo\TreeRoot
     * @ORM\Column(name="root", type="integer", nullable=true)
     */
    private $root;

    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="ChFile", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $parent;

    /**
     * @Expose
     * @ORM\OneToMany(targetEntity="ChFile", mappedBy="parent")
     * @ORM\OrderBy({"lft" = "ASC"})
     */
    private $children;


    /**
     * @ORM\ManyToMany(targetEntity="ShareLink", inversedBy="files")
     */
    public $sharedFile;

    function __construct() {
        $this->isDirectory = false;
    }

    /**
     * @param File $file
     * @return ChFile
     */
    public function setFile($file)
    {
        $this->file = $file;

        if ($this->file) {
            $this->updatedAt = new \DateTime('now');
            $this->fileSize = filesize($this->file);
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $this->mime = finfo_file($finfo, $this->file);
            finfo_close($finfo);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }


    /**
     * @param string $fileName
     * @return ChFile
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
        $this->name = substr(strstr($fileName,'_'), 1);
        $this->uniqId = strstr($fileName,'_',true);

        return $this;
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param \DateTime $updatedAt
     * @return ChFile
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }


    /**
     * @param string $uniqId
     * @return ChFile
     */
    public function setUniqId($uniqId)
    {
        $this->uniqId = $uniqId;

        return $this;
    }

    /**
     * @return string
     */
    public function getUniqId()
    {
        return $this->uniqId;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setParent(ChFile $parent = null)
    {
        $this->parent = $parent;
    }

    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set fileSize
     *
     * @param integer $fileSize
     * @return ChFile
     */
    public function setFileSize($fileSize)
    {
        $this->fileSize = $fileSize;

        return $this;
    }

    /**
     * Get fileSize
     *
     * @return integer 
     */
    public function getFileSize()
    {
        return $this->fileSize;
    }


    /**
     * Set isDirectory
     *
     * @param boolean $isDirectory
     * @return ChFile
     */
    public function setIsDirectory($isDirectory)
    {
        $this->isDirectory = $isDirectory;

        return $this;
    }

    /**
     * Get isDirectory
     *
     * @return boolean 
     */
    public function getIsDirectory()
    {
        return $this->isDirectory;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ChFile
     */
    public function setName($name)
    {
        $this->name = $name;
        $this->updatedAt = new \DateTime('now');

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set user
     *
     * @param \AllanBrault\CoreBundle\Entity\User $user
     * @return ChFile
     */
    public function setUser(\AllanBrault\CoreBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AllanBrault\CoreBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Get mime
     *
     * @return string
     */
    public function getMime()
    {
        if (!$this->isDirectory) {
            return $this->mime;
        }
        return false;
    }


    /**
     * Add sharedFile
     *
     * @param \AllanBrault\CoreBundle\Entity\ShareLink $sharedFile
     * @return ChFile
     */
    public function addSharedFile(\AllanBrault\CoreBundle\Entity\ShareLink $sharedFile)
    {
        $this->sharedFile[] = $sharedFile;

        return $this;
    }

    /**
     * Remove sharedFile
     *
     * @param \AllanBrault\CoreBundle\Entity\ShareLink $sharedFile
     */
    public function removeSharedFile(\AllanBrault\CoreBundle\Entity\ShareLink $sharedFile)
    {
        $this->sharedFile->removeElement($sharedFile);
    }

    /**
     * Get sharedFile
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSharedFile()
    {
        return $this->sharedFile;
    }
}
