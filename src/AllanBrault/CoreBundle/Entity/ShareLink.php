<?php

namespace AllanBrault\CoreBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="sharelink")
 * @ORM\Entity(repositoryClass="AllanBrault\CoreBundle\Repository\ShareLinkRepository")
 */
class ShareLink
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="shareLinkMain")
     */
    public $user;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="shareLinkShared")
     * @ORM\JoinColumn(name="sharedUser", referencedColumnName="id", nullable=true)
     */
    public $sharedUser;

    /**
     * @ORM\ManyToMany(targetEntity="ChFile", inversedBy="sharedFile")
     */
    public $files;

    /**
     * @ORM\Column(type="boolean", name="writable")
     */
    public $writable;

    /**
     * @ORM\Column(type="string", length=255, name="uniqId")
     */
    public $uniqId;


    public function __construct()
    {
        $this->files = new \Doctrine\Common\Collections\ArrayCollection();
        $this->uniqId = uniqid();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set writable
     *
     * @param boolean $writable
     * @return ShareLink
     */
    public function setWritable($writable)
    {
        $this->writable = $writable;

        return $this;
    }

    /**
     * Get writable
     *
     * @return boolean 
     */
    public function getWritable()
    {
        return $this->writable;
    }

    /**
     * Set user
     *
     * @param \AllanBrault\CoreBundle\Entity\User $user
     * @return ShareLink
     */
    public function setUser(\AllanBrault\CoreBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AllanBrault\CoreBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set sharedUser
     *
     * @param \AllanBrault\CoreBundle\Entity\User $sharedUser
     * @return ShareLink
     */
    public function setSharedUser(\AllanBrault\CoreBundle\Entity\User $sharedUser = null)
    {
        $this->sharedUser = $sharedUser;

        return $this;
    }

    /**
     * Get sharedUser
     *
     * @return \AllanBrault\CoreBundle\Entity\User 
     */
    public function getSharedUser()
    {
        return $this->sharedUser;
    }

    /**
     * Add files
     *
     * @param \AllanBrault\CoreBundle\Entity\ChFile $files
     * @return ShareLink
     */
    public function addFile(\AllanBrault\CoreBundle\Entity\ChFile $files)
    {
        $this->files[] = $files;

        return $this;
    }

    /**
     * Remove files
     *
     * @param \AllanBrault\CoreBundle\Entity\ChFile $files
     */
    public function removeFile(\AllanBrault\CoreBundle\Entity\ChFile $files)
    {
        $this->files->removeElement($files);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFiles()
    {
        return $this->files;
    }


    /**
     * Set uniqId
     *
     * @param integer $uniqId
     * @return ShareLink
     */
    public function setUniqId($uniqId)
    {
        $this->uniqId = $uniqId;

        return $this;
    }

    /**
     * Get uniqId
     *
     * @return integer
     */
    public function getUniqId()
    {
        return $this->uniqId;
    }
}
