<?php

namespace AllanBrault\CoreBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="subscription")
 */
class Subscription
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="subscriptions")
     */
    public $user;

    /**
     * @ORM\Column(type="string")
     */
    public $securedArea;

    /**
     * @ORM\Column(type="datetime")
     */
    public $start;

    /**
     * @ORM\Column(type="datetime")
     */
    public $end;

    public function isActive()
    {
        $now = new DateTime();

        return $now >= $this->start && $now < $this->end;
    }

    public function __construct()
    {
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set securedArea
     *
     * @param string $securedArea
     * @return Subscription
     */
    public function setSecuredArea($securedArea)
    {
        $this->securedArea = $securedArea;

        return $this;
    }

    /**
     * Get securedArea
     *
     * @return string 
     */
    public function getSecuredArea()
    {
        return $this->securedArea;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     * @return Subscription
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime 
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set end
     *
     * @param \DateTime $end
     * @return Subscription
     */
    public function setEnd($end)
    {
        $this->end = $end;

        return $this;
    }

    /**
     * Get end
     *
     * @return \DateTime 
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Set user
     *
     * @param \AllanBrault\CoreBundle\Entity\User $user
     * @return Subscription
     */
    public function setUser(\AllanBrault\CoreBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AllanBrault\CoreBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    public function getUserId()
    {
        return $this->user->id;
    }
}
