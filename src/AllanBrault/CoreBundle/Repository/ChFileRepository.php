<?php

namespace AllanBrault\CoreBundle\Repository;

use Gedmo\Tree\Entity\Repository\NestedTreeRepository;
use Doctrine\ORM\Query;

class ChFileRepository extends NestedTreeRepository {

    public function getNonRepeatableName($newName, $user, $chFile = null, $parentId = null, $isFolder = false) {
        $em = $this->getEntityManager();

        $i = 1;
        $ext = '';
        $startName = $newName2 = $newName;
        if($chFile) {
            $isFolder = $chFile->getIsDirectory();
        }
        if(!$isFolder) {
            $ext = pathinfo($newName)['extension'];
            $startName = substr($newName, 0, -(1 + strlen($ext) ));
        }

        if($chFile == null) {
            if($parentId == null || $parentId == 0) {
                $chFilesFolder = $em->createQuery("SELECT f FROM CoreBundle:ChFile f WHERE f.user = :user AND f.parent is NULL ORDER BY f.name ASC")
                    ->setParameters(array(
                        'user' => $user,
                    ))
                    ->getResult();
            } else {
                $chFilesFolder = $em->createQuery("SELECT f FROM CoreBundle:ChFile f WHERE f.user = :user AND f.parent = :parentId ORDER BY f.name ASC")
                    ->setParameters(array(
                        'user' => $user,
                        'parentId' => $parentId
                    ))
                    ->getResult();
            }
        } else if($chFile->getParent() == null) {
            $chFilesFolder = $em->createQuery("SELECT f FROM CoreBundle:ChFile f WHERE f.user = :user AND f.parent is NULL AND f.id != :id ORDER BY f.name ASC")
                ->setParameters(array(
                    'user' => $user,
                    'id' => $chFile->getId()
                ))
                ->getResult();
        } else {
            $chFilesFolder = $em->createQuery("SELECT f FROM CoreBundle:ChFile f WHERE f.user = :user AND f.parent = :parent AND f.id != :id ORDER BY f.name ASC")
                ->setParameters(array(
                    'user' => $user,
                    'parent' => $chFile->getParent(),
                    'id' => $chFile->getId()
                ))
                ->getResult();
        }

        function in_array_r($needle, $haystack) {
            foreach ($haystack as $item) {
                if($item->getName() == $needle) {
                    return true;
                }
            }
            return false;
        }

        while(true) {
            if(in_array_r($newName2, $chFilesFolder)) {
                $newName2 = $startName . ' (' . $i . ')' . '.' . $ext;
                if($isFolder) {
                    $newName2 = $startName . ' (' . $i . ')';
                }
                $i++;
            } else {
                break;
            }
        }

        return $newName2;
    }
}
